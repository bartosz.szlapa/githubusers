package com.example.githubtask.model

import androidx.room.*
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "repo",indices = [Index(value = ["name"], unique = true)])
data class Repo(

    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,

    @Expose
    @SerializedName("name")
    @ColumnInfo(name = "name")

    val name : String = "",

    @Expose
    @SerializedName("owner")
    @Embedded
    var owner : Owner? = null

)
{
    constructor() : this(0,"")
}