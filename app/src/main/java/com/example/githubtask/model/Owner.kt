package com.example.githubtask.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "owner",indices = [Index(value = ["login"], unique = true)])
data class Owner(

    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,

    @Expose
    @SerializedName("login")
    @ColumnInfo(name = "login")
    val login: String = ""
)
{
    constructor() : this(0,"")
}