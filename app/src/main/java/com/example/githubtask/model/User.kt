package com.example.githubtask.model

import androidx.room.*
import com.example.githubtask.db.Converters
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

@Entity(tableName = "user", indices = [Index(value = ["login"], unique = true)])
data class User(

    @PrimaryKey(autoGenerate = true)
    val id : Int? = null ,

    @Expose
    @SerializedName("login")
    @ColumnInfo(name = "login")
    val login : String = "" ,

    @ColumnInfo(name = "avatarUrl")
    @Expose
    @SerializedName("avatar_url")
    val avatarUrl : String = "" ,

    @Expose
    @SerializedName("repos_url")
    @ColumnInfo(name = "reposUrl")
    val reposUrl : String = "",


    @TypeConverters(Converters::class)
    var repos : List<Repo>? = null
    )
{
    constructor() : this(0,"","","",null)
}