package com.example.githubtask.state

import com.example.githubtask.model.Repo
import com.example.githubtask.model.User

data class MainViewState(

    var user : List<User>? = null,
    var repos : List<Repo>? = null

)