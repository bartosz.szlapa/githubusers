package com.example.githubtask.state

sealed class MainStateEvent  {

    class GetUsersEvent  : MainStateEvent()
    class GetReposEvent(val userId : String)  : MainStateEvent()
    class None : MainStateEvent()

}