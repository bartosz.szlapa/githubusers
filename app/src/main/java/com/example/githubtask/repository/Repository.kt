package com.example.githubtask.repository

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.example.githubtask.db.UserDao
import com.example.githubtask.db.UserDatabase
import com.example.githubtask.model.Repo
import com.example.githubtask.model.User
import com.example.githubtask.network.RetrofitBuilder
import com.example.githubtask.state.MainViewState
import com.example.githubtask.util.ApiSuccessResponse
import com.example.githubtask.util.DataState
import com.example.githubtask.util.GenericApiResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.security.auth.login.LoginException

object Repository {

    private var db: UserDatabase? = null
    private var userDao: UserDao? = null

    fun getUsers(): LiveData<DataState<MainViewState>> {
        return object : NetworkBoundResource<List<User>, MainViewState>() {
            override fun createCall(): LiveData<GenericApiResponse<List<User>>> {
                return RetrofitBuilder.apiService.getUser()
            }

            override fun handleApiSuccessResponse(response: ApiSuccessResponse<List<User>>) {
                result.value = DataState.data(
                    message = null,
                    data = MainViewState(
                        user = response.body
                    )
                )
            }
        }.asLiveData()
    }

    fun getRepos(userId: String): LiveData<DataState<MainViewState>> {
        return object : NetworkBoundResource<List<Repo>, MainViewState>() {
            override fun createCall(): LiveData<GenericApiResponse<List<Repo>>> {
                return RetrofitBuilder.apiService.getUserRepos(userId)
            }

            override fun handleApiSuccessResponse(response: ApiSuccessResponse<List<Repo>>) {
                result.value = DataState.data(
                    message = null,
                    data = MainViewState(
                        repos = response.body
                    )
                )
            }
        }.asLiveData()
    }

}