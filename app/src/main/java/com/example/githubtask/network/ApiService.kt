package com.example.githubtask.network

import androidx.lifecycle.LiveData
import com.example.githubtask.model.Repo
import com.example.githubtask.model.User
import com.example.githubtask.util.GenericApiResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiService {

    @GET("/users")
    fun getUser(): LiveData<GenericApiResponse<List<User>>>

    @GET("/users/{userName}/repos")
    fun getUserRepos(
        @Path("userName") userName: String
    ): LiveData<GenericApiResponse<List<Repo>>>


}