package com.example.githubtask.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo


class AppManager {

    companion object {
         fun checkIfNetworkAvailable(context: Context): Boolean {
            val connectivityManager =
                context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
            return activeNetwork?.isConnectedOrConnecting == true
        }
    }
}
