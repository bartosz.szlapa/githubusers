package com.example.githubtask.ui

import android.app.Application
import androidx.lifecycle.*
import com.example.githubtask.model.Repo
import com.example.githubtask.model.User
import com.example.githubtask.repository.Repository
import com.example.githubtask.state.MainStateEvent
import com.example.githubtask.state.MainStateEvent.*
import com.example.githubtask.state.MainViewState
import com.example.githubtask.util.AbsentLiveData
import com.example.githubtask.util.AppManager
import com.example.githubtask.util.DataState

class MainViewModel(application: Application) : AndroidViewModel(application) {

    private val _stateEvent: MutableLiveData<MainStateEvent> = MutableLiveData()
    private val _viewState: MutableLiveData<MainViewState> = MutableLiveData()

    val viewState: LiveData<MainViewState>
        get() = _viewState

    val dataState: LiveData<DataState<MainViewState>> = Transformations
        .switchMap(_stateEvent) { stateEvent ->
            handleStateEvent(stateEvent)
        }

    private fun handleStateEvent(stateEvent: MainStateEvent): LiveData<DataState<MainViewState>>{
        println("DEBUG: New StateEvent detected: $stateEvent")
        when(stateEvent){
            is GetUsersEvent -> {
                return Repository.getUsers()
            }
            is GetReposEvent -> {
                return Repository.getRepos(stateEvent.userId)
            }
            is None ->{
                return AbsentLiveData.create()
            }
        }
    }

    fun setUsers(users: List<User>) {
        val update = getCurrentViewStateOrNew()
        update.user = users
        _viewState.value = update
    }


    fun setStateEvent(event: MainStateEvent) {
        _stateEvent.value = event
    }

    private fun getCurrentViewStateOrNew(): MainViewState {
        return viewState.value?.let {
            it
        } ?: MainViewState()
    }



}