package com.example.githubtask.ui

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import com.bumptech.glide.Glide
import com.example.githubtask.R
import com.example.githubtask.model.User
import kotlinx.android.synthetic.main.layout_users_list_item.view.*
import java.lang.ClassCastException

class UsersListAdapter(private val interaction: Interaction? = null) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), Filterable {

    lateinit var filteredUsersList: MutableList<User>

    val DIFF_CALLBACK = object : DiffUtil.ItemCallback<User>() {

        override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem.login == newItem.login
        }

        override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
            return oldItem == newItem
        }

    }
    private val differ = AsyncListDiffer(this, DIFF_CALLBACK)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return UserViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.layout_users_list_item,
                parent,
                false
            ),
            interaction
        )
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is UserViewHolder -> {
                holder.bind(differ.currentList.get(position))
            }
        }
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    fun submitList(list: List<User>) {
        differ.submitList(list)
    }

    class UserViewHolder
    constructor(
        itemView: View,
        private val interaction: Interaction?
    ) : RecyclerView.ViewHolder(itemView) {

        fun bind(item: User) = with(itemView) {
            itemView.setOnClickListener {
                interaction?.onItemSelected(adapterPosition, item)
            }

            itemView.login.text = item.login
            Glide.with(itemView.context)
                .load(item.avatarUrl)
                .into(itemView.avatar)
        }
    }

    interface Interaction {
        fun onItemSelected(position: Int, item: User)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charString = constraint.toString()
                if (charString.isEmpty())
                    filteredUsersList = differ.currentList
                else {
                    val filteredList = mutableListOf<User>()
                    differ.currentList.map { user ->
                        user.login?.let {
                            if (it.toLowerCase().contains(charString.toLowerCase()))
                                filteredList.add(user)
                        }
                    }

                    filteredUsersList = filteredList
                }

                val filterResults = FilterResults()
                filterResults.values = filteredUsersList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                filteredUsersList = results?.values as MutableList<User>
                println("Debug filtered data : ${filteredUsersList.size}")
                submitList(filteredUsersList)

            }

        }
    }
}

