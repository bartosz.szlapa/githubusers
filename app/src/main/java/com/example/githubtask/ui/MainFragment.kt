package com.example.githubtask.ui

import android.content.Context
import android.os.Bundle
import android.service.autofill.UserData
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.githubtask.R
import com.example.githubtask.db.UserDao
import com.example.githubtask.db.UserDatabase
import com.example.githubtask.model.Repo
import com.example.githubtask.model.User
import com.example.githubtask.state.MainStateEvent
import com.example.githubtask.util.AppManager
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.Dispatchers.Main
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.lang.ClassCastException
import java.lang.Exception

class MainFragment : Fragment(), UsersListAdapter.Interaction {

    lateinit var viewModel: MainViewModel
    lateinit var dataStateListener: DataStateListener
    lateinit var usersListAdapter: UsersListAdapter
    lateinit var reposListAdapter: UserRepoAdapter
    private var db: UserDatabase? = null
    private var userDao: UserDao? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = activity?.run {
            ViewModelProvider(this).get(MainViewModel::class.java)
        } ?: throw Exception("Invalid activity")
        setUsersList()
        subscribeObservers()
        initRecyclerView()
        initReposRecyclerView()
        initSearchView()

    }

    private fun setUsersList() {
        if (AppManager.checkIfNetworkAvailable(context!!))
            triggerGetUsersEvent()
        else
            getUsersFromDb(context!!)
    }

    private fun triggerGetUsersEvent() {
        viewModel.setStateEvent(MainStateEvent.GetUsersEvent())
    }

    private fun triggerGetReposEvent(userId: String) {
        viewModel.setStateEvent(MainStateEvent.GetReposEvent(userId))
    }


    private fun initRecyclerView() {
        recycler_view.apply {
            layoutManager = LinearLayoutManager(activity)
            usersListAdapter = UsersListAdapter(this@MainFragment)
            adapter = usersListAdapter
        }
    }

    private fun initReposRecyclerView() {
        repos_recycler_view.apply {
            layoutManager = LinearLayoutManager(activity)
            reposListAdapter = UserRepoAdapter()
            adapter = reposListAdapter
        }
    }

    private fun initSearchView() {
        search_view.setOnQueryTextListener(object : SearchView.OnQueryTextListener,
            androidx.appcompat.widget.SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                usersListAdapter.filter.filter(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                usersListAdapter.filter.filter(newText)
                println("Debug filter query : $newText")
                if (newText.isNullOrEmpty()) {
                    if (AppManager.checkIfNetworkAvailable(context!!))
                        triggerGetUsersEvent()
                    else
                        getUsersFromDb(context!!)
                }
                return false
            }
        })
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        try {
            dataStateListener = context as DataStateListener
        } catch (e: ClassCastException) {
            println("DEBUG: $context must implement DataStateListener")
        }
    }

    private fun subscribeObservers() {
        viewModel.dataState.observe(viewLifecycleOwner, Observer { dataState ->
            println("DEBUG DataState : $dataState")
            //handle loading and message
            dataStateListener.onDataStateChange(dataState)
            //handle data
            dataState.data?.let { event ->
                event.getContentIfNotHandled()?.let {
                    it.user?.let {
                        usersListAdapter.submitList(it)
                        viewModel.setUsers(it)
                        context?.let { it1 -> insertUsersIntoDb(it1, it) }
                    }

                    it.repos?.let {
                        println("Debug : Reos size ${it.size}")
                        setReposView(it)
                    }

                }
            }
            //handle Error
            dataState?.message?.let {
            }
            //handle loading
            dataState.loading.let {
            }
        })
        viewModel.viewState.observe(viewLifecycleOwner, Observer { mainViewState ->
            mainViewState.user?.let {
                println("Debug : Setting user data to the view")
            }
        })
    }

    private fun setReposView(repos: List<Repo>) {
        if (repos.size > 3) {
            val repTaken = repos.take(3)
            print("REPO TAKEN $repTaken")
            reposListAdapter.submitList(repTaken)
        } else
            reposListAdapter.submitList(repos)
    }

    override fun onItemSelected(position: Int, item: User) {
        item.login?.let {
            triggerGetReposEvent(item.login)
        }

    }

    private fun insertUsersIntoDb(context: Context, users: List<User>) {
        db = UserDatabase.getAppDataBase(context)
        userDao = db?.userDao()
        GlobalScope.launch {
            users.map { user ->
                with(userDao) {
                    this?.insertUser(user)
                }
            }
        }

    }

    private fun getUsersFromDb(context: Context) {
        db = UserDatabase.getAppDataBase(context)
        userDao = db?.userDao()
        GlobalScope.launch {
            val users = db?.userDao()?.getUsersList()
            users?.let {
                withContext(Main) {
                    usersListAdapter.submitList(users)
                    viewModel.setUsers(users)
                }
            }

        }
    }

}