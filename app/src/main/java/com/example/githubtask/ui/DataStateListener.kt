package com.example.githubtask.ui

import com.example.githubtask.util.DataState

interface DataStateListener {

    fun onDataStateChange(dataState: DataState<*>?)
}