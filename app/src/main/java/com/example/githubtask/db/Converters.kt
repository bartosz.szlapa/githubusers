package com.example.githubtask.db

import androidx.room.TypeConverter
import com.example.githubtask.model.Repo
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


class Converters {
    @TypeConverter
    fun fromCountryLangList(countryLang: List<Repo?>?): String? {
        if (countryLang == null) {
            return null
        }
        val gson = Gson()
        val type =
            object : TypeToken<List<Repo?>?>() {}.type
        return gson.toJson(countryLang, type)
    }

    @TypeConverter
    fun toCountryLangList(countryLangString: String?): List<Repo>? {
        if (countryLangString == null) {
            return null
        }
        val gson = Gson()
        val type =
            object : TypeToken<List<Repo?>?>() {}.type
        return gson.fromJson<List<Repo>>(countryLangString, type)
    }

}