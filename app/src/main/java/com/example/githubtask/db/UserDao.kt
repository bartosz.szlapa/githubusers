package com.example.githubtask.db

import androidx.room.*
import com.example.githubtask.model.User

@Dao
interface UserDao {

    @Query("Select * from user")
    fun getUsersList() : List<User>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user : User)

    @Update
    fun updateUser(user : User)

    @Delete
    fun deleteUser(user : User)

}